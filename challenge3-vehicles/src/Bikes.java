public class Bikes extends Vehicle {
    Integer wheel_count;

    public Bikes(String name, String with_engine, String type, Integer wheel_count) {
        super(name, with_engine, type);
        this.wheel_count = wheel_count;
    }

    public Integer getWheel_count() {
        return wheel_count;
    }

    @Override
    public void identity_myself() {
        super.identity_myself();
        System.out.printf(", I have %d Wheel(s)", wheel_count);
    }
}
