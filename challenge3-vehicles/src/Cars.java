public class Cars extends Vehicle {
    Integer wheel_count;
    String engine_type;

    public Cars(String name, String with_engine, String type, Integer wheel_count, String engine_type) {
        super(name, with_engine, type);
        this.wheel_count = wheel_count;
        this.engine_type = engine_type;
    }

    public Integer getWheel_count() {
        return wheel_count;
    }

    public String getEngine_type() {
        return engine_type;
    }

    @Override
    public void identity_myself() {
        super.identity_myself();
        System.out.printf(", I have %d Wheels(s), My Engine Type = %s", getWheel_count(), getEngine_type());
    }
}
