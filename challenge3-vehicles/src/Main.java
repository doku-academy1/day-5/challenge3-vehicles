public class Main {
    public static void main(String[] args) {
        Cars cars = new Cars("xenia", "with engine", "Car", 4, "v8");
        cars.identity_myself();
        System.out.println();

        Bikes bikes = new Bikes("polygon", "no engine", "Bike", 2);
        bikes.identity_myself();
        System.out.println();

        Buses buses = new Buses("primajasa", "with engine", "Bus", 4, "public bus");
        buses.identity_myself();
    }
}