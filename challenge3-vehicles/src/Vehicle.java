public class Vehicle implements Identity {
    public String name;
    public String with_engine;

    public String type;

    public Vehicle(String name, String with_engine, String type) {
        this.name = name;
        this.with_engine = with_engine;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getWith_engine() {
        return with_engine;
    }

    public String getType() {
        return type;
    }

    @Override
    public void identity_myself() {
        System.out.printf("Hi I'm %s, My name is %s, My Engine Status is %s", getType(), getName(), getWith_engine());
    }
}
