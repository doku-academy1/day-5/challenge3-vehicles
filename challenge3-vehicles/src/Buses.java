public class Buses extends Vehicle {
    Integer wheel_count;
    String private_bus;

    public Buses(String name, String with_engine, String type, Integer wheel_count, String private_bus) {
        super(name, with_engine, type);
        this.wheel_count = wheel_count;
        this.private_bus = private_bus;
    }

    public Integer getWheel_count() {
        return wheel_count;
    }

    public String getPrivate_bus() {
        return private_bus;
    }

    @Override
    public void identity_myself() {
        System.out.printf("Hi %s [%s], My name is %s, My Engine Status is %s, I have %d Wheels(s)", getType(), getPrivate_bus(), getName(), getWith_engine(), getWheel_count());
    }
}
